package net.talviuni;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * Clarion Date is a way to describe a date as an integer of days elapsed since
 * 1800-12-28. Kinda like Unix epoch, but not really used in any non-legacy
 * thing.
 *
 */
public class ClarionDateCalculator {
	private static LocalDate START = LocalDate.of(1800, 12, 28);

	public static long clarionDate(LocalDate date) {
		return START.until(date, ChronoUnit.DAYS);
	}

	public static void main(final String[] args) {
		if (args.length == 0) {
			System.out.println("No input given, using LocalDate.now()");
			System.out.println(clarionDate(LocalDate.now()));
			return;
		}

		final String firstArg = args[0];
		try {
			LocalDate date = LocalDate.parse(firstArg);
			System.out.println("Parsed date: " + date.toString());
			System.out.println(clarionDate(date));
		} catch (DateTimeException unused) {
			System.out.println("Input not parseable as a date: " + firstArg);
			System.exit(1);
		}
	}
}
