package net.talviuni;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class ClarionDateCalculatorTest {
	
	private static LocalDate START= LocalDate.of(1800, 12, 28); 

	@Test
	public void testStartDateGivesZero() {

		long diff = ClarionDateCalculator.clarionDate(START);

		assertEquals(0, diff);
	}

	@Test
	public void testStartDatePlusOneGivesOne() {
		long diff = ClarionDateCalculator.clarionDate(START.plusDays(1));

		assertEquals(1, diff);
	}
	
	@Test
	public void testHundredThousandDaysAreConsecutive() {
		for(int i =0; i <= 100_000; i++) {
			final LocalDate date = START.plusDays(i); 
			long diff = ClarionDateCalculator.clarionDate(date);
			
			assertEquals("Mismatch on date " + date.toString(),i, diff); 
		}
		
	}
}
